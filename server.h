#pragma once
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#include "sensors.h"

#define SSID "._."
#define PASSWORD "Maximus3000"

ESP8266WebServer server(80);
const IPAddress ip(10,0,0,75);
const IPAddress gateway(10,0,0,1);
const IPAddress subnet(255,255,255,0);
const IPAddress dns(8,8,8,8);

const char* hostname = "cannaview.local";
const char* ssid     = SSID;
const char* password = PASSWORD;

const char* json_template = "{\"temperature\":%.2f,\
\"humidity\":%.2f,\
\"co2\":%hu}";

void handle_data();
void handle_root();
char* data_as_json(float* dht_data, unsigned short co2_ppm);

void setup_server() {
    WiFi.disconnect();
    WiFi.mode(WIFI_STA);
    WiFi.config(ip, gateway, subnet, dns);
    WiFi.hostname(hostname);
    WiFi.begin(ssid, password);

    Serial.print(F("Connecting"));

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.print(F(" Connected, IP address: "));
    Serial.println(WiFi.localIP());

    if (MDNS.begin(F("esp8266"))) {
        Serial.println(F("MDNS responder started"));
    } else {
        Serial.println(F("Error setting up MDNS responder!"));
    }

    server.on("/", handle_root);
    server.on("/data", HTTP_GET, handle_data);
    server.begin();
}

void handle_root() {
    server.send(200, "text/html", "<h1>CannaView Server</h1>");
}

void handle_data() {
    server.send(200, "application/json", data_as_json(dht_data, co2_ppm));
}

char* data_as_json(float* dht_data, unsigned short co2_ppm) {
    char* data_str = new char[50];
    sprintf(data_str, json_template, dht_data[0], dht_data[1], co2_ppm);
    Serial.println(data_str);

    return data_str;
}