#pragma once
#include <Wire.h>
#include <SparkFunCCS811.h>
#include <DHT.h>

#define DHTPIN 12
#define CCS811_ADDRESS 0x5A

DHT dht(DHTPIN, DHT11);
CCS811 sensor(CCS811_ADDRESS);

unsigned long last_read = 0;
float* dht_data = new float[2];
unsigned short co2_ppm;

//time elapsed
int g_hours, g_minutes, g_seconds;

void setup_sensors() {
    Wire.begin();

    if (sensor.begin() == false) {
        Serial.println(F("CCS811 failed to start"));
        while(1);
    }
    else
    {
        Serial.println(F("CCS811 started"));
    }

    dht.begin();
    Serial.println(F("Sensors initialized"));
}

void get_dht_data() {
    float t = dht.readTemperature(true);
    float h = dht.readHumidity();

    if (isnan(t) || isnan(h)) {
        Serial.println(F("Failed to read from DHT sensor!"));
        return;
    }

    if (t != dht_data[0] || h != dht_data[1]) {
        dht_data[0] = t;
        dht_data[1] = h;
    }

    Serial.print(F("Temperature: "));
    Serial.print(t);
    Serial.print(F("F, Humidity: "));
    Serial.print(h);
    Serial.print(F("%, "));
}

void get_co2_data() {
    if (sensor.dataAvailable()) {
        sensor.readAlgorithmResults();
        co2_ppm = sensor.getCO2();
    } else {
        Serial.println(F("No CO2 data available, "));
    }

    Serial.print(F("CO2: "));
    Serial.println(co2_ppm);
}

void get_time() {
    unsigned long time = millis() / 1000;
    
    Serial.print(F("Time: "));

    if (time >= 60) {
        if (time / 3600 > 0) {
            g_hours = time / 3600;
            Serial.print(g_hours);
            Serial.print(F("h "));
            time %= 3600;
        }

        if (time / 60 > 0) {
            g_minutes = time / 60;
            Serial.print(g_minutes);
            Serial.print(F("m "));
            time %= 60;
        }
    }

    if (time > 0) {
        g_seconds = time;
        Serial.print(g_seconds);
        Serial.print(F("s, "));
    }
}

void get_data() {
    get_time();
    get_dht_data();
    get_co2_data();
    last_read = millis();
}