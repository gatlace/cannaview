#include "sensors.h"
#include "server.h"

#define SENS_INTERVAL 5000

void setup() {
    Serial.begin(9600);
    Serial.println(F("ESP Web Server"));

    setup_server();

    setup_sensors();
}

void loop() {
    if (millis() - last_read > SENS_INTERVAL) {
        get_data();
    }

    server.handleClient();
}


